import { Routes } from '@angular/router';

// import { LoginComponent } from '../dishdetail/dishdetail.component';
import { HomeComponent } from '../home/home.component';
// import { AboutComponent } from '../about/about.component';
// import { ContactComponent } from '../contact/contact.component';

export const routes: Routes = [
  { path: 'home',  component: HomeComponent },
  // { path :'contactus', component: ContactComponent},
  // { path: 'about', component: AboutComponent},
  // { path: 'login', component: LoginComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];
